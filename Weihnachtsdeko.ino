#include <Adafruit_NeoPixel.h>

//#define DEBUG

#define PIN           6  // Arduino pin (Digital PWM) that the LED strip is connected to
#define LEDS          10 // Number of LEDs to address

#define MAX           50 // Maximal brightness value
#define MIN           2  // Minimal brightness value
#define MAX_STEPPING  3
#define MIN_STEPPING  1
#define DELAY         30 // Delay in milliseconds
#define MIN_OFF_TICKS 1  // Number of loops for which an LED must stay off
#define MAX_OFF_TICKS 10 // Number of loops for which an LED can stay off
#define WARMTH        98 // Percentage of the BLUE value compared to RED and GREEN

float brightness[LEDS];
float stepping[LEDS];
int   goingUp[LEDS];
int   offTicks[LEDS];

int brightnessA, brightnessB;

Adafruit_NeoPixel strip = Adafruit_NeoPixel(LEDS, PIN, NEO_GRB + NEO_KHZ800);

void setup() {
  #ifdef DEBUG
    Serial.begin(9600);
  #endif
  
  strip.begin();

  for(int i=0; i < strip.numPixels(); i++) {
    initPixel(i);
    randomizeStepping(i);
  }

  strip.show();
}
 
void loop() {
  if(random(0, 100) > 90) {
    randomizeStepping(random(0, LEDS));
  }
  
  for(int i = 0; i < strip.numPixels(); i++) {

    if(offTicks[i] > 0) {
      offTicks[i]--;
      continue;
    }
    
    if(goingUp[i]) {
      brightness[i] += stepping[i];

      if(brightness[i] > MAX) {
        goingUp[i] = 0;
        brightness[i] = MAX;
      }
    } else {
      brightness[i] -= stepping[i];

      if(brightness[i] < MIN) {
        goingUp[i] = 1;
        brightness[i] = MIN;
        offTicks[i] = random(MIN_OFF_TICKS, MAX_OFF_TICKS);
      }
    }
    
    updatePixel(i);
  }

  #ifdef DEBUG
    debugPixel(0);
  #endif

  strip.show();
  
  delay(DELAY);
}

static void initPixel(int pixel) {
  brightness[pixel] = random(MIN, MAX);
  goingUp[pixel] = (int) random(0, 2);
}

static void randomizeStepping(int pixel) {
  Serial.println(pixel);
  stepping[pixel] = random(MIN_STEPPING * 1000, MAX_STEPPING * 1000) / 1000.0;
}

static void updatePixel(int pixel) {
  brightnessA = brightness[pixel];
  brightnessB = brightnessA * (float)WARMTH / 100.0;
    
  strip.setPixelColor(pixel, brightnessA, brightnessA, brightnessB);
}

#ifdef DEBUG
  static void debugPixel(int pixel) {
    Serial.print(brightness[pixel]);
    Serial.print(" \t");
    Serial.print(stepping[pixel]);
    Serial.print(" \t");
    Serial.print(goingUp[pixel]);
    Serial.println();
  }
#endif

